Regular Expression Cookbook
===================
	Some people, when confronted with a problem, think 
	'I know, I'll use regular expressions.'
	Now they have two problems.



### Special characters

Character| Function
--------| ---
**Basic** |
`^`		| match row beginning (text beginning in **single line mode**)
`$`		| match row ending(text finish in **single line mode**)
`\`		| mask for special characters
`.`		| match any character
`l` (pipe)		| Separates alternate possibilities.
`\n`		| new line
`\t`		| tabulator
`\b`		| word boundary
`\w`		| Matches an alphanumeric character, same as **[A-Za-z0-9_]**
`\W`		|Matches a non-alphanumeric character, excluding **"\_"**, same as **[^A-Za-z0-9_]**
`\d` 		| Numbers, same as **[0-9]**
`\D` 		| Not numbers, same as **[^0-9]**
`\s` 		| Whitespace
`\S` 		| Not whitespace
**Special** |
`\p{P}` 	| punctuations
`\p{L}` 	| letters
`\p{Lu}` 	| big letters
`\p{Ll}` 	| small letters
`\p{N}` 	| numbers
`\p{Z}` 	| serparators
**Repetition** |
`RegExp*`	| 0 or more occurrences (other form **RegEx{0,}**)
`RegExp+`	| 1 or more occurrences (other form **RegEx{1,}**)
`RegExp?`	| 0 or 1 occurrence (other form **RegEx{0,1}**)
`RegExp{min,max}`	| can occur from **min** to **max** occurrence
**Grouping** |
`[ ]` | Denotes a set of possible character matches.
`( )` | Groups a series of pattern elements to a single element
`^` | negation in groups(on the beginning of group)
**Backreferences** |
`\1` | Refer to not named groups (where number 1 is an index)
`?< name >`	|	Define named backreference
`\k< name >`	|	Refer to named backreference
`$&`	|	Refer to whole match
`$_`	|	Refer to input text
`` $` ``	|	Text before match
`$'`	|	Text after match
**(positive/negative) look(ahead/behind)** |
`(?=RegExp)` | positive lookahead
`(?!RegExp)` | negative lookahead
`(?<=RegExp)` | positive lookbehind
`(?<!RegExp)` | negative lookbehind