[TOC]

#Basic
## Example 1: find all occurrences of basic Regexp
**Problem:** Find all occurences of sequence: **a(any char)c**.

**Input:**

```
This is a text with squence examples abc, aac, abb, acb, a.b, a.c
```

**Pattern:** 	


```
a.c
```

**Result:** 	
```
abc
aac
a.c
```

#Notepad++
## Example 1: remove duplicate lines
**Problem:** Find duplicated lines in sorted sequence of lines.

**Input:**

```
a.c
aac
ab
abc
abc
abc
abcd
abcd
edf
edf
edffgg
```

**Pattern:** 	


Find:
```
^(.*\r?\n)\1
```
Replace:
```
\1
```

**Result:** 	
```
a.c
aac
ab
abc
abcd
edf
edffgg
```
## Example 2: remove duplicate lines
**Problem:** Find duplicated lines in unsorted sequence of lines.

**Input:**

```
abc
aac
a.c
ab
abc
abc
abcd
abcd
edf
edf
edffgg
```

**Pattern:** 	


Find:
```
^(.*?)$\s+?^(?=.*^\1$)
```
Replace:
```

```

**Result:** 	
```
aac
a.c
ab
abc
abcd
edf
edffgg
```

## Example 3: remove duplicated words in text
**Problem:** Find duplicated lines in unsorted sequence of lines.

**Input:**

```
Lorem Ipsum is is simply dummy text of the printing and and typesetting industry. 
It has survived not not only five centuries, but but also the leap into 
electronic typesetting, remaining essentially unchanged.
```

**Pattern:** 	


Find:
```
\s(\w+)(?=\s\1)
```
Replace:
```

```

**Result:** 	
```
Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
It has survived not only five centuries, but also the leap into 
electronic typesetting, remaining essentially unchanged.
```